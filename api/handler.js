'use strict';

const AWS = require('aws-sdk');
let dynamo = new AWS.DynamoDB.DocumentClient();

require('aws-sdk/clients/apigatewaymanagementapi'); 

const TABLE_NAME = 'chatIdTable';

exports.connectionHandler = async event => {
  let promise = addConnection;
  let type = 'Connect';
  if (event.requestContext.eventType === 'DISCONNECT') {
    promise = deleteConnection;  
    type = 'Disconnect';
  }

  try {
    await promise(event.requestContext.connectionId);
  } catch (error) {
    return {
      statusCode: 500,  
      body: `Failed ${JSON.stringify(error)}`
    };
  }

  return {
    statusCode: 200,
    body: 'everything is alright'
  };
};

// THIS ONE DOESNT DO ANYHTING
exports.defaultHandler = async _ => {
  console.log('defaultHandler was called');

  return {
    statusCode: 200,
    body: 'defaultHandler'
  };
};

exports.sendMessageHandler = (event, _context, callback) => {
  sendMessageToAllConnected(event)
    .then(() => {
      callback(null, { statusCode: 200, body: 'everything is alright' });
    })
    .catch(error => {
      callback(null, { statusCode: 500, body: `Failed ${error.stack}`});
    })
}

const sendMessageToAllConnected = async event => {
  const connectionData = await getConnectionIds();
  return connectionData.Items.map(connectionId => send(event, connectionId.connectionId));
}

const getConnectionIds = _ => dynamo.scan({
    TableName: TABLE_NAME,
    ProjectionExpression: 'connectionId'
  }
).promise();

const send = async (event, connectionId) => {
  const body = JSON.parse(event.body);
  const { data } = body;

  const endpoint = event.requestContext.domainName + '/' + event.requestContext.stage;
  const apigwManagementApi = new AWS.ApiGatewayManagementApi({
    apiVersion: "2018-11-29",
    endpoint: endpoint
  });

  try {
    await apigwManagementApi.postToConnection({
      ConnectionId: connectionId,
      Data: data // JSON.stringify({ data })
    }).promise();
  } catch (e) {
    if (e.statusCode === 410) {
      console.log(`Found stale connection, deleting ${connectionId}`);
      await deleteConnection(connectionId);
    } else {
      throw e;
    }
  }
};

const addConnection = connectionId => dynamo.put({
    TableName: TABLE_NAME,
    Item: {
      connectionId
    }
  }
).promise();

const deleteConnection = connectionId => dynamo.delete({
    TableName: TABLE_NAME,
    Key: {
      connectionId
    }
  }
).promise();
