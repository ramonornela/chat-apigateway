import React, { useEffect } from 'react';
import { Chat } from './pages';
import './App.css';
import { ReactComponent as Logo } from './logo-unip.svg';
import Amplify from 'aws-amplify';
import { AmplifyAuthenticator, AmplifyGreetings, AmplifySignUp, AmplifySignIn } from '@aws-amplify/ui-react';
import { AuthState, onAuthUIStateChange } from '@aws-amplify/ui-components';
import awsconfig from './aws-exports';
import './translations';

Amplify.configure(awsconfig);

function App() {
  const [authState, setAuthState] = React.useState();
  const [user, setUser] = React.useState();

  useEffect(() => {
    return onAuthUIStateChange((nextAuthState, authData) => {
      setAuthState(nextAuthState);
      setUser(authData);
    });
  }, []);

  return (
    <>
      <div className={ authState === AuthState.SignedIn && user ? 'logo-login' : 'logo'}>
        <Logo />
      </div>
      {authState === AuthState.SignedIn && user ? (
        <div className="app">
          <AmplifyGreetings className="app--greetings" username={user.username}></AmplifyGreetings>
          <Chat />
        </div>
      ) : (
        <AmplifyAuthenticator>
          <AmplifySignUp
            slot="sign-up"
            usernameAlias="email"
            formFields={[
              {
                type: "name",
                label: "Nome",
                placeholder: "Nome",
                required: true,
              },
              {
                type: "email",
                label: "Email",
                placeholder: "Email",
                required: true,
              },
              {
                type: "password",
                label: "Password",
                placeholder: "Password",
                required: true,
              },
              {
                type: "phone_number",
                label: "Telefone",
                placeholder: "Phone Number",
                required: false,
              },
            ]}
          />
          <AmplifySignIn slot="sign-in" usernameAlias="email" />
        </AmplifyAuthenticator>
      )}
    </>
  );
}

export default App;
