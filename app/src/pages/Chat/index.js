import React, { useCallback, useEffect, useRef, useState  } from 'react';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import './chat.css';

export function Chat() {
    const {
        sendJsonMessage,
        lastMessage,
        readyState,
    } = useWebSocket('wss://liqru2rmxi.execute-api.us-east-1.amazonaws.com/dev');

    const refTextarea = useRef(null);
    
    const [message, setMessage] = useState('');
    const [messages, addMessage] = useState([]);

    const connectionStatus = {
        [ReadyState.CONNECTING]: 'Conectando ...',
        [ReadyState.OPEN]: 'Aberto',
        [ReadyState.CLOSING]: 'Fechando ...',
        [ReadyState.CLOSED]: 'Fechado',
        [ReadyState.UNINSTANTIATED]: 'Instável',
    }[readyState];

    const handleSendMessage = useCallback(() => {
        sendJsonMessage({ action: 'sendMessage', data: message });
    }, [message, sendJsonMessage]);

    useEffect(() => {
        if (refTextarea && refTextarea.current) {
            refTextarea.current.value = '';
        }

        if (lastMessage?.data) {
            addMessage(previous => [...previous, lastMessage?.data]);
        }
    }, [lastMessage?.data, refTextarea]);
    
    return (
        <>
            <header className="Chat-header">
                <h1>
                    Chat simples com ApiGateway
                </h1>
                <p>Status: {connectionStatus}</p>
            </header>

            <main className="Chat-main">
                <div className="Chat-main--text">
                    <textarea ref={refTextarea} placeholder="Digita aqui sua mensagem ..." onChange={ev => setMessage(ev.target.value)}></textarea>
                    <div>
                        <button
                            onClick={handleSendMessage}
                            disabled={readyState !== ReadyState.OPEN}
                        >
                            Enviar mensagem
                        </button>
                    </div>
                </div>
                <div className="Chat-main--message">
                    {messages.length > 0 && (
                        <>
                            <ul>
                            {messages.map((m, i) => <li key={i}>{m}</li>)}
                            </ul>
                        </>
                    )}
                </div>
            </main>
        </>
    );
}
