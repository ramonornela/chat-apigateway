import { I18n } from "aws-amplify";
import { Translations } from "@aws-amplify/ui-components";

I18n.putVocabulariesForLanguage("en-US", {
  [Translations.SIGN_IN_HEADER_TEXT]: "APS",
  [Translations.SIGN_IN_ACTION]: "Login",
  [Translations.EMAIL_LABEL]: "E-mail *",
  [Translations.PASSWORD_LABEL]: "Senha *",
  [Translations.FORGOT_PASSWORD_TEXT]: "Esqueceu sua senha?",
  [Translations.RESET_PASSWORD_TEXT]: "Mudar senha",
  [Translations.NO_ACCOUNT_TEXT]: "Sem conta?",
  [Translations.CREATE_ACCOUNT_TEXT]: "Criar Conta",
  [Translations.CONFIRM_SIGN_UP_CODE_LABEL]: "Código de confirmação",
  [Translations.CONFIRM_SIGN_UP_CODE_PLACEHOLDER]: "",
  [Translations.CONFIRM_SIGN_UP_HEADER_TEXT]: "",
  [Translations.CONFIRM_SIGN_UP_LOST_CODE]: "",
  [Translations.CONFIRM_SIGN_UP_RESEND_CODE]: "",
  [Translations.EMAIL_PLACEHOLDER]: "Digite seu e-mail",
  [Translations.PASSWORD_PLACEHOLDER]: "Digite sua senha",
  [Translations.SIGN_UP_HEADER_TEXT]: "Criar uma nova conta",
  [Translations.RESET_YOUR_PASSWORD]: "Resetar sua senha",
  [Translations.USERNAME_LABEL]: "Usuário",
  [Translations.USERNAME_PLACEHOLDER]: "Digite seu nome de usuário",
  [Translations.SEND_CODE]: "Enviar código",
  [Translations.BACK_TO_SIGN_IN]: "Voltar para o login",
  [Translations.SIGN_UP_HAVE_ACCOUNT_TEXT]: "Você possui conta?",
  [Translations.PHONE_LABEL]: "Telefone",
  [Translations.SIGN_UP_EMAIL_PLACEHOLDER]: "E-mail",
  [Translations.SIGN_UP_PASSWORD_PLACEHOLDER]: "Senha",
  [Translations.VERIFY_CONTACT_PHONE_LABEL]: "Telefone",
  [Translations.CODE_LABEL]: "Código de verificação",
  [Translations.CODE_PLACEHOLDER]: "Digite o código",
  [Translations.NEW_PASSWORD_LABEL]: "Nova senha",
  [Translations.NEW_PASSWORD_PLACEHOLDER]: "Digite a nova senha",
  [Translations.SIGN_IN_ACTION]: "Login",
  [Translations.SIGN_IN_TEXT]: "Login",
  [Translations.CREATE_ACCOUNT_TEXT]: "Criar conta",
  [Translations.CONFIRM_SIGN_UP_CODE_LABEL]: "Código de confirmação",
  [Translations.CONFIRM_SIGN_UP_CODE_PLACEHOLDER]: "Digite o código de verificação",
  [Translations.CONFIRM_SIGN_UP_HEADER_TEXT]: "Confirmar conta",
  [Translations.CONFIRM_SIGN_UP_LOST_CODE]: "Perdeu o seu código?",
  [Translations.CONFIRM_SIGN_UP_RESEND_CODE]: "Reenviar código",
  [Translations.SIGN_UP_SUBMIT_BUTTON_TEXT]: "Criar conta",
  [Translations.CONFIRM]: "Confirmar",
  [Translations.SIGN_OUT]: "Logout"
});

/*
 AuthStrings["BACK_TO_SIGN_IN"] = "Back to Sign In";
  AuthStrings["CHANGE_PASSWORD_ACTION"] = "Change";
  AuthStrings["CHANGE_PASSWORD"] = "Change Password";
  AuthStrings["CODE_LABEL"] = "Verification code";
  AuthStrings["CODE_PLACEHOLDER"] = "Enter code";
  AuthStrings["CONFIRM_SIGN_UP_CODE_LABEL"] = "Confirmation Code";
  AuthStrings["CONFIRM_SIGN_UP_CODE_PLACEHOLDER"] = "Enter your code";
  AuthStrings["CONFIRM_SIGN_UP_HEADER_TEXT"] = "Confirm Sign up";
  AuthStrings["CONFIRM_SIGN_UP_LOST_CODE"] = "Lost your code?";
  AuthStrings["CONFIRM_SIGN_UP_RESEND_CODE"] = "Resend Code";
  AuthStrings["CONFIRM_SIGN_UP_SUBMIT_BUTTON_TEXT"] = "Confirm";
  AuthStrings["CONFIRM_SMS_CODE"] = "Confirm SMS Code";
  AuthStrings["CONFIRM_TOTP_CODE"] = "Confirm TOTP Code";
  AuthStrings["CONFIRM"] = "Confirm";
  AuthStrings["CREATE_ACCOUNT_TEXT"] = "Create account";
  AuthStrings["EMAIL_LABEL"] = "Email Address *";
  AuthStrings["EMAIL_PLACEHOLDER"] = "Enter your email address";
  AuthStrings["FORGOT_PASSWORD_TEXT"] = "Forgot your password?";
  AuthStrings["LESS_THAN_TWO_MFA_VALUES_MESSAGE"] = "Less than two MFA types available";
  AuthStrings["NEW_PASSWORD_LABEL"] = "New password";
  AuthStrings["NEW_PASSWORD_PLACEHOLDER"] = "Enter your new password";
  AuthStrings["NO_ACCOUNT_TEXT"] = "No account?";
  AuthStrings["USERNAME_REMOVE_WHITESPACE"] = "Username cannot contain whitespace";
  AuthStrings["PASSWORD_REMOVE_WHITESPACE"] = "Password cannot start or end with whitespace";
  AuthStrings["PASSWORD_LABEL"] = "Password *";
  AuthStrings["PASSWORD_PLACEHOLDER"] = "Enter your password";
  AuthStrings["PHONE_LABEL"] = "Phone Number *";
  AuthStrings["PHONE_PLACEHOLDER"] = "(555) 555-1212";
  AuthStrings["QR_CODE_ALT"] = "qrcode";
  AuthStrings["RESET_PASSWORD_TEXT"] = "Reset password";
  AuthStrings["RESET_YOUR_PASSWORD"] = "Reset your password";
  AuthStrings["SELECT_MFA_TYPE_HEADER_TEXT"] = "Select MFA Type";
  AuthStrings["SELECT_MFA_TYPE_SUBMIT_BUTTON_TEXT"] = "Verify";
  AuthStrings["SEND_CODE"] = "Send Code";
  AuthStrings["SUBMIT"] = "Submit";
  AuthStrings["SETUP_TOTP_REQUIRED"] = "TOTP needs to be configured";
  AuthStrings["SIGN_IN_ACTION"] = "Sign In";
  AuthStrings["SIGN_IN_HEADER_TEXT"] = "Sign in to your account";
  AuthStrings["SIGN_IN_TEXT"] = "Sign in";
  AuthStrings["SIGN_IN_WITH_AMAZON"] = "Sign in with Amazon";
  AuthStrings["SIGN_IN_WITH_AUTH0"] = "Sign in with Auth0";
  AuthStrings["SIGN_IN_WITH_AWS"] = "Sign in with AWS";
  AuthStrings["SIGN_IN_WITH_FACEBOOK"] = "Sign in with Facebook";
  AuthStrings["SIGN_IN_WITH_GOOGLE"] = "Sign in with Google";
  AuthStrings["SIGN_OUT"] = "Sign Out";
  AuthStrings["SIGN_UP_EMAIL_PLACEHOLDER"] = "Email";
  AuthStrings["SIGN_UP_HAVE_ACCOUNT_TEXT"] = "Have an account?";
  AuthStrings["SIGN_UP_HEADER_TEXT"] = "Create a new account";
  AuthStrings["SIGN_UP_PASSWORD_PLACEHOLDER"] = "Password";
  AuthStrings["SIGN_UP_SUBMIT_BUTTON_TEXT"] = "Create Account";
  AuthStrings["SIGN_UP_USERNAME_PLACEHOLDER"] = "Username";
  AuthStrings["SUCCESS_MFA_TYPE"] = "Success! Your MFA Type is now:";
  AuthStrings["TOTP_HEADER_TEXT"] = "Scan then enter verification code";
  AuthStrings["TOTP_LABEL"] = "Enter Security Code:";
  AuthStrings["TOTP_ISSUER"] = "AWSCognito";
  AuthStrings["TOTP_SETUP_FAILURE"] = "TOTP Setup has failed";
  AuthStrings["TOTP_SUBMIT_BUTTON_TEXT"] = "Verify Security Token";
  AuthStrings["TOTP_SUCCESS_MESSAGE"] = "Setup TOTP successfully!";
  AuthStrings["UNABLE_TO_SETUP_MFA_AT_THIS_TIME"] = "Failed! Unable to configure MFA at this time";
  AuthStrings["USERNAME_LABEL"] = "Username *";
  AuthStrings["USERNAME_PLACEHOLDER"] = "Enter your username";
  AuthStrings["VERIFY_CONTACT_EMAIL_LABEL"] = "Email";
  AuthStrings["VERIFY_CONTACT_HEADER_TEXT"] = "Account recovery requires verified contact information";
  AuthStrings["VERIFY_CONTACT_PHONE_LABEL"] = "Phone Number";
  AuthStrings["VERIFY_CONTACT_SUBMIT_LABEL"] = "Submit";
  AuthStrings["VERIFY_CONTACT_VERIFY_LABEL"] = "Verify";
  AuthStrings["ADDRESS_LABEL"] = "Address";
  AuthStrings["ADDRESS_PLACEHOLDER"] = "Enter your address";
  AuthStrings["NICKNAME_LABEL"] = "Nickname";
  AuthStrings["NICKNAME_PLACEHOLDER"] = "Enter your nickname";
  AuthStrings["BIRTHDATE_LABEL"] = "Birthday";
  AuthStrings["BIRTHDATE_PLACEHOLDER"] = "Enter your birthday";
  AuthStrings["PICTURE_LABEL"] = "Picture URL";
  AuthStrings["PICTURE_PLACEHOLDER"] = "Enter your picture URL";
  AuthStrings["FAMILY_NAME_LABEL"] = "Family Name";
  AuthStrings["FAMILY_NAME_PLACEHOLDER"] = "Enter your family name";
  AuthStrings["PREFERRED_USERNAME_LABEL"] = "Preferred Username";
  AuthStrings["PREFERRED_USERNAME_PLACEHOLDER"] = "Enter your preferred username";
  AuthStrings["GENDER_LABEL"] = "Gender";
  AuthStrings["GENDER_PLACEHOLDER"] = "Enter your gender";
  AuthStrings["PROFILE_LABEL"] = "Profile URL";
  AuthStrings["PROFILE_PLACEHOLDER"] = "Enter your profile URL";
  AuthStrings["GIVEN_NAME_LABEL"] = "First Name";
  AuthStrings["GIVEN_NAME_PLACEHOLDER"] = "Enter your first name";
  AuthStrings["ZONEINFO_LABEL"] = "Time zone";
  AuthStrings["ZONEINFO_PLACEHOLDER"] = "Enter your time zone";
  AuthStrings["LOCALE_LABEL"] = "Locale";
  AuthStrings["LOCALE_PLACEHOLDER"] = "Enter your locale";
  AuthStrings["UPDATED_AT_LABEL"] = "Updated At";
  AuthStrings["UPDATED_AT_PLACEHOLDER"] = "Enter the time the information was last updated";
  AuthStrings["MIDDLE_NAME_LABEL"] = "Middle Name";
  AuthStrings["MIDDLE_NAME_PLACEHOLDER"] = "Enter your middle name";
  AuthStrings["WEBSITE_LABEL"] = "Website";
  AuthStrings["WEBSITE_PLACEHOLDER"] = "Enter your website";
  AuthStrings["NAME_LABEL"] = "Full Name";
  AuthStrings["NAME_PLACEHOLDER"] = "Enter your full name";
  AuthStrings["PHOTO_PICKER_TITLE"] = "Picker Title";
  AuthStrings["PHOTO_PICKER_HINT"] = "Ancillary text or content may occupy this space here";
  AuthStrings["PHOTO_PICKER_PLACEHOLDER_HINT"] = "Placeholder hint";
  AuthStrings["PHOTO_PICKER_BUTTON_TEXT"] = "Button";
  AuthStrings["IMAGE_PICKER_TITLE"] = "Add Profile Photo";
  AuthStrings["IMAGE_PICKER_HINT"] = "Preview the image before upload";
  AuthStrings["IMAGE_PICKER_PLACEHOLDER_HINT"] = "Tap to image select";
  AuthStrings["IMAGE_PICKER_BUTTON_TEXT"] = "Upload";
  AuthStrings["PICKER_TEXT"] = "Pick a file";
  AuthStrings["TEXT_FALLBACK_CONTENT"] = "Fallback Content";
  AuthStrings["CONFIRM_SIGN_UP_FAILED"] = "Confirm Sign Up Failed";
  AuthStrings["SIGN_UP_FAILED"] = "Sign Up Failed";
  */
 