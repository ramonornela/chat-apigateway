
const config = {
    aws_project_region: 'us-east-1',
    aws_cognito_identity_pool_id: `${process.env.REACT_APP_IDENTITY_POOL_ID}`,
    aws_cognito_region: 'us-east-1',
    aws_user_pools_id: `${process.env.REACT_APP_POOLS_ID}`,
    aws_user_pools_web_client_id: `${process.env.REACT_APP_POOLS_WEB_CLIENT_ID}`,
    aws_appsync_authenticationType: 'AMAZON_COGNITO_USER_POOLS'
};

export default config;
